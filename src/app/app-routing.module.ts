import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchBooksComponent } from './pages/search-books/search-books.component';
import { ReviewComponent } from './pages/review/review.component';

const routes: Routes = [
  {path: '', component: SearchBooksComponent}, 
  {path: 'review', component:ReviewComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
