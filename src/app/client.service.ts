import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  //se inyecta el cliente http de Angular
  constructor(private http: HttpClient) { }

  getRequestConsultBooks(route: string){
    //configuracion del tipo de respuesta esperado
    let config: any = {
      responseType: "json"
    };

    const header = new HttpHeaders().set('Authorization', '57ydf544ljka559ahjkfgd1');
    config["header"] = header;

    return this.http.get(route, config);
  }

  postRequestSendReview(route: string, data: any){
    //configuracion del tipo de respuesta esperado
    let config: any = {
      responseType: "json"
    };

    const header = new HttpHeaders().set('Authorization', '57ydf544ljka559ahjkfgd1');
    config["header"] = header;

    return this.http.post(route, data, config);
  }
}
