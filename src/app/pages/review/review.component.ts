import { Component, OnInit } from '@angular/core';
//importacion de servicios
import { ClientService } from '../../client.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  books!:Array<any>;
  book:any = 0;
  review!:string;

  constructor(private client:ClientService) { }

  ngOnInit(): void {
    this.showBooks();
  }

  showBooks(){
    this.client.getRequestConsultBooks("http://localhost:10101/consultBooks").subscribe(
      (response: any) => {
        console.log(response);
        this.books = response;
      },
      (error) => {
        console.log(error.status);
      }
    );
  }

  sendReview(){
    console.log(this.book);
    console.log(this.review);

    this.client.postRequestSendReview("http://localhost:10101/review/add",{
      descripcion: this.review,
      idLibro: this.book
    }).subscribe(
      (response: any) => {
        console.log(response);
      },
      (error) => {
        console.log(error.status);
      }
    );

    this.review = '';
  }
}
