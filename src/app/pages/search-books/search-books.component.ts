import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/app/client.service';

@Component({
  selector: 'app-search-books',
  templateUrl: './search-books.component.html',
  styleUrls: ['./search-books.component.css']
})
export class SearchBooksComponent implements OnInit {

  books!:Array<any>;
  param!:string;

  isbn!:number;
  author!:string;
  title!:string;

  constructor(private client:ClientService) { }

  ngOnInit(): void {
    this.showBooks();
  }

  showBooks(){
    this.client.getRequestConsultBooks("http://localhost:10101/consultBooks").subscribe(
      (response: any) => {
        this.books = response;
        console.log(response);
        
      },
      (error) => {
        console.log(error.status);
      }
    );
  }

  showBooksParam(){
    console.log(`http://localhost:10101/consultBooks/${this.param}`);
    this.client.getRequestConsultBooks(`http://localhost:10101/consultBooks/${this.param}`).subscribe(
      (response: any) => {
        this.books = response;
        console.log(response);
        
      },
      (error) => {
        console.log(error.status);
      }
    );
  }

  showBooksParams(){
    console.log(this.isbn);
    console.log(this.author);
    console.log(`http://localhost:10101/consultBooks/${this.isbn}/${this.author}/${this.title}`);
    
    this.client.getRequestConsultBooks(`http://localhost:10101/consultBooks/${this.isbn}/${this.author}/${this.title}`).subscribe(
      (response: any) => {
        this.books = response;
        console.log(response);
        
      },
      (error) => {
        console.log(error.status);
      }
    );
  }
}
